import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';

import Header from '../Header/Header.js';
import MainContent from '../MainContent/MainContent.js';
import Footer from '../Footer/Footer.js';
import Popup from '../Popup/Popup.js';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            shouldShowPopup: false
        };

        this.togglePopup = this.togglePopup.bind(this);
    }

    togglePopup(state) {
        this.setState({
            shouldShowPopup: state
        })
    }

    render() {
        return (
            <div className="app">
                <Header/>
                <MainContent onClick={this.togglePopup}/>
                <Footer/>
                { this.state.shouldShowPopup && <Popup onClick={this.togglePopup}/> }
            </div>
        );
    }
}

export default App;
