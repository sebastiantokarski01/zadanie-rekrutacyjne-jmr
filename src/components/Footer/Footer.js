import React, { Component } from 'react';
import './Footer.scss';

class Footer extends Component {
    render() {
        return (
            <footer className="site-footer">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-sm-4 justify-content-start align-items-center">
                            <span className="copyright text-uppercase">&copy; 2014 Raspberry kingdom</span>
                        </div>
                        <div className="col-12 col-sm-4 justify-content-center align-items-center">
                            <a className="cookies" href="/cookies">Cookies</a>
                            <span className="separator">|</span>
                            <a className="privacy" href="/privacy">Privacy</a>
                        </div>
                        <div className="col-12 col-sm-4 justify-content-end align-items-center">
                            <span className="info">Design by <strong>Wizard of Oz</strong></span>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;