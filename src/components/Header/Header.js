import React, { Component } from 'react';
import logo from "../../images/raspberry.png";
import './Header.scss';

class Header extends Component {

    /**
     * Makes navigation visible or hidden if someone clicks hamburger button
     * @param {event} e
     */
    handleClick(e) {
        const btn = e.target.nodeName === 'SPAN' ? e.target.parentNode : e.target;
        const navbar = document.querySelector(btn.dataset.target);

        if (navbar.classList.contains('visible-group')) {
            navbar.classList.remove('visible-group');
        } else {
            navbar.classList.add('visible-group');
        }
    }

    render() {
        return (
            <header className="site-header">
                <div className="container">
                    <div className="row">
                        <img className="logo" src={logo} alt="logo"/>
                        <nav className="nav navbar navbar-dark justify-content-center">
                            <button className="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
                                    aria-expanded="false" aria-label="Toggle navigation"
                                    onClick={this.handleClick}>
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <ul className="nav-group" id="navbarNavAltMarkup">
                                <li className="nav-item">
                                    <a className="nav-link text-uppercase" href="/about">About</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link text-uppercase" href="/offer">Offer</a>
                                </li>
                                <li className="nav-item space-before">
                                    <a className="nav-link text-uppercase" href="/gallery">Gallery</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link text-uppercase" href="/contact">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
        );
    }
}

export default Header;