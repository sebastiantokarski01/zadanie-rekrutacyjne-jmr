import React, { Component } from 'react';
import './MainContent.scss';

class MainContent extends Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.onClick(true);
    }

    render() {
        return (
            <main className="site-content">
                <div className="container">
                    <div className="row justify-content-center">
                        <h1 className="title text-center">Raspberry kingdom</h1>
                    </div>
                    <div className="row justify-content-center">
                        <button className="btn btn-lg btn-login text-uppercase" onClick={this.handleClick}>Enter the gates</button>
                    </div>
                </div>
            </main>
        );
    }
}

export default MainContent;