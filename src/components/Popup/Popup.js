import React, { Component } from 'react';
import './Popup.scss';

class Popup extends Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleClick() {
        this.props.onClick(false);
    }

    handlePopupClick(e) {
        e.stopPropagation();
    }

    handleSubmit(e) {
        e.preventDefault();
        const data = {
            login: e.target.email.value,
            password: e.target.password.value
        };

        // Checking only falsy values
        if (data.login && data.password) {
            this.sendLoginRequest(data);
        }
    }

    /**
     *
     * @param {Object} data
     */
    sendLoginRequest(data) {
        fetch('https://recruitment-api.pyt1.stg.jmr.pl/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(
                (result) => {
                    let popupTitle = document.querySelector('.popup-title');
                    if (result.status === 'ok') {
                        popupTitle.textContent = 'Yes, you are Raspberry Knight :)';
                    } else {
                        popupTitle.textContent = 'No, You are not Raspberry Knight';
                    }
                },
                (error) => {
                    console.error('sendLoginRequest error:', error);
                }
            )
    }

    render() {
        return (
            <div className="layer" onClick={this.handleClick}>
                <div className="popup" onClick={this.handlePopupClick}>
                    <form className="popup-inner input-group" action="" onSubmit={this.handleSubmit}>
                        <p className="popup-title text-center">Are you a Raspberry Knight?</p>
                        <input className="popup-input input-email" type="text" name="email" placeholder="Email"/>
                        <input className="popup-input input-password" type="password" name="password" placeholder="Password" />
                        <button className="btn popup-submit" type="submit">LOG IN</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Popup;